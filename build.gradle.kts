/*
 * Copyright 2024 Mark Slater
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
plugins {
    kotlin("jvm") version "1.9.20"
    `java-gradle-plugin`
    id("com.gradle.plugin-publish") version "1.2.1"
}

repositories {
    mavenCentral()
}

version = "1.3"
group = "com.gitlab.svg2ico"

tasks {
    compileKotlin {
        kotlinOptions {
            jvmTarget = "1.8"
        }
    }
}

dependencies {
    implementation(group = "net.sourceforge.svg2ico", name = "svg2ico", version = "1.55")
}

testing {
    @Suppress("UnstableApiUsage")
    suites {
        val test by getting(JvmTestSuite::class) {
            useJUnitJupiter()
            dependencies {
                implementation("org.junit.jupiter:junit-jupiter-api:5.10.2")
                implementation("io.kotest:kotest-assertions-core:5.8.0")
            }
        }
    }
}

@Suppress("UnstableApiUsage")
gradlePlugin {
    website.set("https://gitlab.com/svg2ico/svg2ico-gradle-plugin")
    vcsUrl.set("https://gitlab.com/svg2ico/svg2ico-gradle-plugin.git")
    plugins {
        create("svg2IcoPlugin") {
            id = "com.gitlab.svg2ico"
            implementationClass = "com.gitlab.svg2ico.Svg2IcoPlugin"
            displayName = "svg2ico plugin"
            description = "Converts SVG images into ICO and PNG"
            tags.set(listOf("svg", "ico", "png"))
        }
    }
}